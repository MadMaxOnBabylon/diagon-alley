// Login/Registration switch

var loginForm = document.getElementById("loginForm");
var registrationForm = document.getElementById("registrationForm");
var indicator = document.getElementById("indicator");


function login() {
    registrationForm.style.transform = "translateX(300px)";
    loginForm.style.transform = "translateX(300px)";
    indicator.style.transform = "translateX(0)";
}

function register() {
    registrationForm.style.transform = "translateX(0px)";
    loginForm.style.transform = "translateX(0px)";
    indicator.style.transform = "translateX(100px)";
}